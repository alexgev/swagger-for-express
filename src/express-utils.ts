import { NextFunction, Request, RequestHandler, Response } from "express";
import { ParamsDictionary, Query } from "express-serve-static-core";
import { Schema, Validator } from "jsonschema";
import { JsonObject } from "swagger-ui-express";

export const runAsyncWrapper = <
  P = ParamsDictionary,
  ResBody = any,
  ReqBody = any,
  ReqQuery = Query
>(
  handler: (
    ...args: Parameters<RequestHandler<P, ResBody, ReqBody, ReqQuery>>
  ) => Promise<void>
): RequestHandler<P, ResBody, ReqBody, ReqQuery> => {
  return function (req, res, next) {
    handler(req, res, next).catch(next);
  };
};

export const ExpressUtils = {
  runAsyncWrapper,
};

export const validateMiddleware = (
  schema: Schema,
  dataStore: "body" | "query"
) =>
  ExpressUtils.runAsyncWrapper(
    async (req: Request, res: Response, next: NextFunction) => {
      var v = new Validator();

      const result = v.validate(req[dataStore], schema);
      if (result.errors.length > 0) {
        console.log("result", result);

        res.status(400).send(result.errors);
        return;
      }
      next();
    }
  );

export const createSchema = (
  method: "post" | "get",
  path: string,
  summary: string,
  successResponseSchema: Schema,
  errorResponseSchema: Schema,
  requestSchema?: Schema
) => {
  let dataObj = null;
  if (requestSchema) {
    switch (method) {
      case "get":
        if (requestSchema.properties) {
          dataObj = {
            parameters: Object.entries(requestSchema.properties).map(
              (keyValue) => {
                const [key, value] = keyValue;
                return {
                  in: "query",
                  name: key,
                  schema: {
                    type: value.type,
                    minLength: value.minLength,
                    maxLength: value.maxLength,
                  },
                  description: value.description,
                };
              }
            ),
          };
        }
        break;
      case "post":
        dataObj = {
          requestBody: {
            required: true,
            content: {
              "application/json": {
                schema: requestSchema,
              },
            },
          },
        };
        break;
      default:
        const a: never = method;
        throw new Error('Method "' + a + '" not supported');
    }
  }
  return {
    [path]: {
      [method]: {
        summary,
        ...dataObj,
        responses: {
          200: {
            description: "Successful response",
            content: {
              "application/json": {
                schema: successResponseSchema,
              },
            },
          },
          500: {
            description: "Error response",
            content: {
              "application/json": {
                schema: errorResponseSchema,
              },
            },
          },
        },
      },
    },
  };
};
