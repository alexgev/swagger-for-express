import { Router } from "express";
import { UserCreateBodySchema, UserCreateSuccessSchema } from "./schema";
import { userCreate } from "./handler";
import { validateMiddleware } from "../express-utils";

export const userCreateRouter = (
  router: Router,
  handler: typeof userCreate
) => {
  router.post<
    Record<string, string>,
    UserCreateSuccessSchema,
    UserCreateBodySchema,
    Record<never, never>
  >(
    "/user/create",
    validateMiddleware(UserCreateBodySchema, "body"),

    async (req, res) => {
      const body = req.body;

      const result = await handler(body.name);

      res.send(result);
    }
  );
};
