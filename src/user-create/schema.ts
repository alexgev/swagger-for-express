import { Static, Type } from "@sinclair/typebox";
import { createSchema } from "../express-utils";

export const UserCreateBodySchema = Type.Object(
  {
    name: Type.String({ minLength: 1 }),
    age: Type.Number({ minimum: 1 }),
  },
  {
    additionalProperties: false,
    title: "UserCreateBodySchema Schema",
    description: "UserCreateBodySchema Schema",
  }
);

export type UserCreateBodySchema = Static<typeof UserCreateBodySchema>;

export const UserCreateSuccessSchema = Type.Object(
  {
    name: Type.String(),
    id: Type.Number(),
  },
  {
    additionalProperties: false,
    title: "UserCreateSuccessSchema Schema",
    description: "UserCreateSuccessSchema Schema",
  }
);

export type UserCreateSuccessSchema = Static<typeof UserCreateSuccessSchema>;

export const UserCreateErrorSchema = Type.Object(
  {
    errorCode: Type.Number(),
    description: Type.String(),
  },
  {
    additionalProperties: false,
    title: "UserCreateErrorSchema Schema",
    description: "UserCreateErrorSchema Schema",
  }
);

export type UserCreateErrorSchema = Static<typeof UserCreateErrorSchema>;

export const UserCreateSchema = createSchema(
  "post",
  "/user/create",
  "Create new user",
  UserCreateSuccessSchema,
  UserCreateErrorSchema,
  UserCreateBodySchema
);
