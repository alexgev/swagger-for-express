import bodyParser from "body-parser";
import express, { Router } from "express";

import swaggerUi from "swagger-ui-express";
import { userCreateRouter } from "./user-create";
import { apiSpec } from "./api-spec";
import { userCreate } from "./user-create/handler";
import { userGetRouter } from "./user-get";
import { userGet } from "./user-get/handler";
import util from "util";

const app = express();

const router = Router();

// # define routes
userCreateRouter(router, userCreate);
userGetRouter(router, userGet);

app.use(
  bodyParser.json({
    limit: "2mb",
  })
);
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "2mb",
  })
);

app.use(router);

app.use("/api-docs", swaggerUi.serve);
app.get("/api-docs", swaggerUi.setup(apiSpec));

app.listen(3000, () => {
  console.log("server started");
});
