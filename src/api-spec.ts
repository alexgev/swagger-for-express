import { UserCreateSchema } from "./user-create/schema";
import { UserGetSchema } from "./user-get/schema";

export const apiSpec = {
  openapi: "3.0.0",
  info: {
    title: "Sample API Spec",
    version: "1.0.0",
  },
  servers: [
    {
      url: "http://localhost:3000",
    },
  ],
  paths: {
    ...UserCreateSchema,
    ...UserGetSchema
  },
};
