import { Router } from "express";
import { validateMiddleware } from "../express-utils";
import { userGet } from "./handler";
import { UserGetQuerySchema, UserGetSuccessSchema } from "./schema";

export const userGetRouter = (
  router: Router,
  handler: typeof userGet
) => {
  router.get<
    Record<string, string>,
    UserGetSuccessSchema,
    Record<never, never>,
    UserGetQuerySchema
  >(
    "/user/get",
    validateMiddleware(UserGetQuerySchema, "query"),

    async (req, res) => {
      const query = req.query;

      const result = await handler(query.id);

      console.log('result', result);
      

      res.send(result);
    }
  );
};
