import { Static, Type } from "@sinclair/typebox";
import { createSchema } from "../express-utils";

export const UserGetQuerySchema = Type.Object(
  {
    id: Type.String({ minLength: 1 }),
  },
  {
    additionalProperties: false,
    title: "UserGetQuerySchema Schema",
    description: "UserGetQuerySchema Schema",
  }
);

export type UserGetQuerySchema = Static<typeof UserGetQuerySchema>;

export const UserGetSuccessSchema = Type.Object(
  {
    id: Type.String(),
  },
  {
    additionalProperties: false,
    title: "UserGetSuccessSchema Schema",
    description: "UserGetSuccessSchema Schema",
  }
);

export type UserGetSuccessSchema = Static<typeof UserGetSuccessSchema>;

export const UserGetErrorSchema = Type.Object(
  {
    errorCode: Type.Number(),
    description: Type.String(),
  },
  {
    additionalProperties: false,
    title: "UserGetErrorSchema Schema",
    description: "UserGetErrorSchema Schema",
  }
);

export type UserGetErrorSchema = Static<typeof UserGetErrorSchema>;

export const UserGetSchema = createSchema(
  "get",
  "/user/get",
  "Get user",
  UserGetSuccessSchema,
  UserGetErrorSchema,
  UserGetQuerySchema
);
