# Swagger for express (typescript)

Library based on [typebox](https://github.com/sinclairzx81/typebox), [jsonschema](https://github.com/tdegrunt/jsonschema#readme) and [swagger-ui-express](https://github.com/scottie1984/swagger-ui-express)

## Usage
- Define schemas by Typebox (then we can use types for typechecking in controllers and schemas for swagger)
- Define generics for express router by schemas (it helps us to avoid misnaming in controller and handler return type)
- Define express middleware for validation body or query by schemas
- Format global api spec